import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CCCloseButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: 20,
          left: 20,
        }}
        onPress={this.props.action}
      >
        <Icon name="close" size={30} />
      </TouchableOpacity>
    );
  }
}
