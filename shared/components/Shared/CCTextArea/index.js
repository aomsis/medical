import React, { Component } from 'react';
import { TextInput } from 'react-native';

import styles from './style';
import { style } from '../../../assets/styles/main';

export default class CCTextArea extends Component {
  render() {
    return (
      <TextInput
        multiline={true}
        style={[styles.input, this.props.style]}
        onChangeText={e => this.props.handleTextChange(e)}
        value={this.props.value}
        placeholder={this.props.placeholder}
        maxLength={244}
        underlineColorAndroid="rgba(0,0,0,0)"
        blurOnSubmit={true}
        placeholderTextColor={style.LIGHT_GRAY}
        keyboardAppearance="dark"
      />
    );
  }
}
