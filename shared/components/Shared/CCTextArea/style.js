import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export default StyleSheet.create({
  input: {
    height: 70,
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: style.FONT_SIZE_S,
    fontFamily: style.FONT_OPEN_SANS,
    color: style.BLACK,
    backgroundColor: style.WHITE,
    borderRadius: 2,
  },
});
