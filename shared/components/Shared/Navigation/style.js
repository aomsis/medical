import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

// just a placeholder component so contains bad vars
export const styles = StyleSheet.create({
  navigationClosed: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  navigation: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '80%',
    height: '100%',
    flex: 1,
    backgroundColor: style.TRANSPARENT_BLACK_DARK,
    paddingTop: 45,
    zIndex: 2,
  },
  navigationWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    flex: 1,
  },
  bottom: {
    position: 'absolute',
    bottom: 30,
    left: 30,
  },
  hamburger: {
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  line: {
    width: 18,
    height: 2,
    backgroundColor: style.WHITE,
    marginTop: 3,
  },
  logo: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 50,
    resizeMode: 'contain',
  },
  menu: {
    width: '100%',
    height: '100%',
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  item: {
    fontSize: style.FONT_SIZE_XXXL,
    paddingVertical: 15,
    width: '100%',
  },
  itemGray: {
    fontSize: style.FONT_SIZE_XL,
    paddingVertical: 15,
    width: '100%',
    color: style.LIGHT_GRAY,
  },
  menuClosed: {
    display: 'none',
  },
  disabled: {
    color: style.GRAY,
  },
});

export default styles;
