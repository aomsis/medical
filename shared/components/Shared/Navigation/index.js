import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Linking,
  Keyboard,
  StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { logout } from '../../../actions/users';

import { requestLocation } from '../../../utils/locationTracking';

import CCText from '../CCText';

import { styles } from './style';

export class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.handleRequestRide = this.handleRequestRide.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentWillReceiveProps(nextProps) {}

  toggleMenu() {
    Keyboard.dismiss();
    this.setState({ open: !this.state.open });
  }

  handleContactUs() {
    Actions.contact();
  }

  handleHelp() {
    Actions.help();
  }

  handleSettings() {
    Actions.settings();
  }

  handleRequestRide() {
    Actions.home();
  }

  handleLogout() {
    Actions.walkthrough();
    this.props.logout();
  }

  get requestARide() {
    return (
      <TouchableOpacity onPress={this.handleRequestRide}>
        <CCText style={styles.item}>首页</CCText>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View
        style={
          this.state.open ? styles.navigationWrapper : styles.navigationClosed
        }
      >
        <StatusBar barStyle="light-content" backgroundColor="#000000" />
        <TouchableOpacity onPress={this.toggleMenu} style={styles.hamburger}>
          <CCText style={styles.line} />
          <CCText style={styles.line} />
          <CCText style={styles.line} />
        </TouchableOpacity>

        <TouchableOpacity
          style={
            this.state.open ? styles.navigationWrapper : styles.navigationClosed
          }
          onPress={this.toggleMenu}
        />
        <View
          style={this.state.open ? styles.navigation : styles.navigationClosed}
        >
          <View style={this.state.open ? styles.menu : styles.menuClosed}>
            {this.requestARide}
            <TouchableOpacity onPress={this.handleHelp}>
              <CCText style={styles.item}>帮助</CCText>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleContactUs}>
              <CCText style={styles.item}>联系我们</CCText>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleSettings}>
              <CCText style={styles.item}>用户信息</CCText>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleLogout}>
              <CCText style={styles.item}>登出</CCText>
            </TouchableOpacity>
            <View style={styles.bottom}>
              <TouchableOpacity
                onPress={() => {
                  Actions.toc();
                }}
              >
                <CCText style={styles.itemGray}>使用条款</CCText>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  Actions.policy();
                }}
              >
                <CCText style={styles.itemGray}>隐私政策</CCText>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

Navigation.propTypes = {
  fromLocation: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  rideBlocked: PropTypes.bool.isRequired,
};

Navigation.defaultProps = {
  fromLocation: {
    id: '',
  },
  rideBlocked: false,
};

const mapStateToProps = state => ({
  auth: state.user.auth,
  currentUser: state.user.currentUser,
  fromLocation: state.locations.fromLocation,
});

export default connect(mapStateToProps, { logout })(Navigation);
