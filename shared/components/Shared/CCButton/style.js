import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  button: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  red: {
    backgroundColor: style.RED,
  },
  yellow: {
    backgroundColor: style.YELLOW,
  },
  gray: {
    backgroundColor: style.GRAY,
  },
  clear: {
    backgroundColor: 'transparent',
  },
  blackText: {
    color: style.BLACK,
  },
  whiteText: {
    color: style.WHITE,
  },
  skyText: {
    color: style.SKY,
  },
  buttonText: {
    color: style.WHITE,
    fontSize: style.FONT_SIZE_L,
    fontFamily: style.FONT_OPEN_SANS,
    letterSpacing: 0.5,
  },
  small: {
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
  },
});
