import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import { styles } from './style';

export default class CCButton extends Component {
  get textColor() {
    switch (this.props.color) {
      case 'yellow':
        return 'blackText';
      case 'red':
      case 'gray':
        return 'whiteText';
      case 'sky':
        return 'skyText';
      case 'transparent':
        return 'transparent';
    }
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.handlePress}>
        <View
          style={[
            styles.button,
            styles[this.props.color],
            styles[this.props.size],
            this.props.style,
          ]}
        >
          <Text style={[styles.buttonText, styles[this.textColor]]}>
            {this.props.label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
