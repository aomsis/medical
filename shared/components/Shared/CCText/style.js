import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export default StyleSheet.create({
  text: {
    fontSize: style.FONT_SIZE_S,
    fontFamily: style.FONT_OPEN_SANS,
    color: style.WHITE,
  },
});
