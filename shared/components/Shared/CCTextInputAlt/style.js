import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export default StyleSheet.create({
  input: {
    height: 45,
    fontSize: style.FONT_SIZE_XXL,
    fontFamily: style.FONT_OPEN_SANS,
    color: style.WHITE,
    borderBottomWidth: 2,
    borderColor: style.TRANSPARENT_WHITE,
    width: '100%',
  },
});
