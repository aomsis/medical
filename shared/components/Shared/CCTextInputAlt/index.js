import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';

import { style } from '../../../assets/styles/main';

import styles from './style';

export default class CCTextInputAlt extends Component {
  render() {
    // TODO: ADD OPTION FOR ICON
    return (
      <TextInput
        style={[styles.input, this.props.style]}
        onChangeText={this.props.handleTextChange}
        value={this.props.value}
        secureTextEntry={this.props.secureTextEntry}
        placeholder={this.props.placeholder}
        placeholderTextColor={this.props.placeholderTextColor}
        onEndEditing={this.props.handleInputBlur}
        editable={this.props.editable}
        keyboardType={this.props.keyboardType}
        maxLength={this.props.maxLength}
        onFocus={this.props.onFocus}
        underlineColorAndroid="rgba(0,0,0,0)"
        keyboardAppearance="dark"
      />
    );
  }
}

CCTextInputAlt.propTypes = {
  editable: PropTypes.bool,
  placeholderTextColor: PropTypes.string,
  keyboardType: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  maxLength: PropTypes.number,
};

CCTextInputAlt.defaultProps = {
  editable: true,
  keyboardType: 'default',
  placeholderTextColor: style.TRANSPARENT_WHITE,
  secureTextEntry: false,
  maxLength: 400,
};
