import { StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export default StyleSheet.create({
  ccTextInput: {
    position: 'relative',
    width: '100%',
  },
  input: {
    height: 45,
    backgroundColor: style.WHITE,
    paddingHorizontal: 15,
    fontSize: style.FONT_SIZE_M,
    fontFamily: style.FONT_OPEN_SANS,
    color: style.DARK_GRAY,
    borderRadius: 2,
  },
  iconWrapper: {
    position: 'absolute',
    top: 0,
    right: 15,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    height: 17,
    width: 17,
    opacity: 0.4,
  },
});
