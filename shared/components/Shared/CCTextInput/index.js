import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, Image } from 'react-native';

import { style } from '../../../assets/styles/main';

import styles from './style';

export default class CCTextInput extends Component {
  render() {
    return (
      <View style={[styles.ccTextInput, this.props.style]}>
        <TextInput
          onFocus={this.props.onFocus}
          onBlur={this.props.onBlur}
          defaultValue={this.props.defaultValue}
          autoFocus={this.props.autoFocus}
          style={[styles.input, this.props.inputStyle]}
          onChangeText={this.props.handleTextChange}
          secureTextEntry={this.props.secureTextEntry}
          value={this.props.value}
          placeholder={this.props.placeholder}
          placeholderTextColor={this.props.placeholderTextColor}
          editable={this.props.editable}
          keyboardType={this.props.keyboardType}
          maxLength={this.props.maxLength}
          underlineColorAndroid="rgba(0,0,0,0)"
          keyboardAppearance="dark"
        />
        {this.props.icon ? (
          <View style={styles.iconWrapper}>
            <Image
              source={require('../../../assets/images/search-icon.png')}
              style={styles.icon}
            />
          </View>
        ) : null}
      </View>
    );
  }
}

CCTextInput.propTypes = {
  editable: PropTypes.bool,
  placeholderTextColor: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  onFocus: PropTypes.func,
  defaultValue: PropTypes.string,
  keyboardType: PropTypes.string,
  autoFocus: PropTypes.bool,
  maxLength: PropTypes.number,
};

CCTextInput.defaultProps = {
  onFocus: () => {},
  autoFocus: false,
  editable: true,
  keyboardType: 'default',
  placeholderTextColor: style.DARK_GRAY,
  secureTextEntry: false,
  maxLength: 400,
};
