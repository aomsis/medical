'use strict'

import React, { Component } from 'react'
import {
  Text,
  View,
} from 'react-native'
import PropTypes from 'prop-types';

const LoadingView = ({
  visible,
}) => {
  return (
    <View>
    </View>
  );
}

LoadingView.propTypes = {
  visible: PropTypes.bool.isRequired,
};

LoadingView.defaultProps = {
  visible: false,
};

LoadingView.displayName = 'LoadingView';

export default LoadingView;
