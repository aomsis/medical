import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/FontAwesome';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  'window'
);

const styles = {
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: 'black',
  },
  textContainer: {
    padding: 20,
    backgroundColor: '#0db26b',
  },
  textContainer1: {
    padding: 20,
    backgroundColor: '#097747',
  },
  textContainer2: {
    padding: 20,
    backgroundColor: '#055432',
  },
  textContainer3: {
    padding: 20,
    backgroundColor: '#9155a7',
  },
  textContainer4: {
    padding: 20,
    backgroundColor: 'white',
  },
  textContainer5: {
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#0db26b',
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    marginVertical: 10,
  },
  titleAlt: {
    color: '#097747',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    marginVertical: 10,
  },
  subtitleAlt: {
    color: '#097747',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginVertical: 10,
  },
  desc: {
    color: 'white',
    fontSize: 16,
  },
  descAlt: {
    color: 'gray',
    fontSize: 16,
  },
  iconContainer: {
    backgroundColor: '#097747',
    width: 80,
    height: 80,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 40,
  },
};

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      entries: [
        {
          url: require('../../../assets/images/slider1.jpg'),
        },
        {
          url: require('../../../assets/images/slider2.jpg'),
        },
        {
          url: require('../../../assets/images/slider3.jpg'),
        },
      ],
    };
  }

  _renderItem({ item, index }) {
    return (
      <Image
        source={item.url}
        indicator={ProgressBar}
        indicatorProps={{ indeterminate: true, color: 'white' }}
        style={{ height: 200, width: viewportWidth }}
        resizeMode="cover"
      />
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.overlay} />
        <ScrollView>
          <View>
            <Carousel
              data={this.state.entries}
              renderItem={this._renderItem.bind(this)}
              sliderWidth={viewportWidth}
              itemWidth={viewportWidth}
              loop
              autoplay
            />
            <View style={styles.textContainer}>
              <Text style={styles.title}>WHY GO BEYOND THE VERBAL</Text>
              <Text style={styles.desc}>
                AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
                HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
                FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS
                ON PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL
                UNDERSTANDING.
              </Text>
            </View>
          </View>
          <View style={styles.textContainer1}>
            <Text style={styles.title}>WHY GO BEYOND THE VERBAL</Text>
            <Text style={styles.desc}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
          </View>
          <View style={styles.textContainer2}>
            <Text style={styles.title}>WHY GO BEYOND THE VERBAL</Text>
            <Text style={styles.desc}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
          </View>
          <View style={styles.textContainer3}>
            <Text style={styles.title}>WHY GO BEYOND THE VERBAL</Text>
            <Text style={styles.desc}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
          </View>
          <View style={styles.textContainer4}>
            <Text style={styles.titleAlt}>TAKING CARE OF YOUR HEALTH</Text>
            <View style={styles.iconContainer}>
              <Icon name="comments" color="white" size={50} />
            </View>
            <Text style={styles.subtitleAlt}>Health Plans We Accept</Text>
            <Text style={styles.descAlt}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
            <View style={styles.iconContainer}>
              <Icon name="user" color="white" size={50} />
            </View>
            <Text style={styles.subtitleAlt}>
              Number 1 Medical Clinic in the Area
            </Text>
            <Text style={styles.descAlt}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
            <View style={styles.iconContainer}>
              <Icon name="user-md" color="white" size={50} />
            </View>
            <Text style={styles.subtitleAlt}>Specialist Doctors</Text>
            <Text style={styles.descAlt}>
              AT BEYOND VERBAL, WE ARE PASSIONATE ABOUT THE POWER OF VOICE. WE
              HAVE DEVELOPED A TECHNOLOGY THAT EXTRACTS VARIOUS ACOUSTIC
              FEATURES FROM A SPEAKER’S VOICE, IN REAL TIME, GIVING INSIGHTS ON
              PERSONAL HEALTH CONDITION, WELLBEING AND EMOTIONAL UNDERSTANDING.
            </Text>
          </View>
          <View style={styles.textContainer5}>
            <Text style={styles.desc}>{'\u00A9'} 2023 by Medical Clinic.</Text>
            <Text style={styles.desc}>Shanghai, China</Text>
            <Text style={styles.desc}>Tel: 123-456-7890</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}
