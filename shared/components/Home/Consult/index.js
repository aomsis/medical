'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, ListView, ScrollView } from 'react-native';
import { List, ListItem, Avatar } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import CCButton from '../../../components/Shared/CCButton';
import { styles, sliderWidth, itemWidth } from './style';
import SliderEntry from './SliderEntry';
import CCText from '../../../components/Shared/CCText';

const RightButtons = props => (
  <TouchableOpacity style={styles.smallButton} onPress={props.handleTreat}>
    <CCText style={styles.smallButtonTitle}>就医</CCText>
  </TouchableOpacity>
);

export default class Consult extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }

  showDoctorInfo(index) {
    const { showDoctor } = this.props;
    showDoctor(index);
  }

  _renderItem({ item, index }, parallaxProps) {
    return (
      <SliderEntry
        data={item}
        even
        parallax
        parallaxProps={parallaxProps}
        showDoctorInfo={() => this.showDoctorInfo(index)}
      />
    );
  }

  renderRow(rowData, sectionID, rowID) {
    const date = new Date(Date.parse(rowData.creation_date));
    const title =
      rowData.patient_name.trim().length !== 0
        ? rowData.patient_name.trim()
        : rowData.phonenumber;
    const { handleConsult, handleTreat, selectRecord } = this.props;
    return (
      <ListItem
        avatar={
          <Avatar
            rounded
            // source={rowData.avatar_url && { uri: rowData.avatar_url }}
            source={{
              uri:
                'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            }}
            title={rowData.patient_name[0]}
          />
        }
        key={rowID}
        title={title}
        subtitle={date ? date.toDateString() : ' '}
        containerStyle={{ borderBottomWidth: 0 }}
        onPress={() => {
          selectRecord(rowID);
        }}
        rightIcon={
          <RightButtons
            handleConsult={handleConsult}
            handleTreat={handleTreat}
          />
        }
      />
    );
  }

  render() {
    const {
      doctors,
      handleSubmit,
      visitRecords,
      consultRecords,
      treatRecords,
    } = this.props;

    return (
      <ScrollView style={styles.wrapper}>
        <View style={styles.contentView}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={doctors}
            renderItem={this._renderItem.bind(this)}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            hasParallaxImages
            inactiveSlideScale={0.94}
            inactiveSlideOpacity={0.7}
            containerCustomStyle={styles.slider}
            contentContainerCustomStyle={styles.sliderContentContainer}
            loopClonesPerSide={2}
          />
          <CCButton
            color="yellow"
            label="支付"
            handlePress={handleSubmit}
            style={styles.payButton}
          />
          <CCText style={styles.title}>出诊记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={visitRecords}
            />
          </List>
          <CCText style={styles.title}>会诊记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={consultRecords}
            />
          </List>
          <CCText style={styles.title}>就医记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={treatRecords}
            />
          </List>
          {/* <CCButton
            color="yellow"
            label="支付"
            handlePress={handleSubmit}
            style={styles.payButton}
          /> */}
        </View>
      </ScrollView>
    );
  }
}
