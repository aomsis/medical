import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import { Avatar } from 'react-native-elements';
import CheckBox from 'react-native-checkbox-heaven';
import { styles, SKY } from './style';

export default class SliderEntry extends Component {
  // static propTypes = {
  //   data: PropTypes.object.isRequired,
  //   even: PropTypes.bool,
  //   parallax: PropTypes.bool,
  //   parallaxProps: PropTypes.object,
  // };

  get image() {
    const {
      data: { illustration },
      parallax,
      parallaxProps,
      even,
    } = this.props;

    return parallax ? (
      <ParallaxImage
        source={require('../../../assets/images/hospital.jpg')}
        containerStyle={[
          styles.imageContainer,
          even ? styles.imageContainerEven : {},
        ]}
        style={styles.image}
        parallaxFactor={0.35}
        showSpinner={true}
        spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
        {...parallaxProps}
      />
    ) : (
      <Image source={{ uri: illustration }} style={styles.image} />
    );
  }

  render() {
    const {
      data: { first_name, last_name, subtitle, avatar },
      even,
      showDoctorInfo,
    } = this.props;

    const uppercaseTitle = (
      <Text
        style={[styles.title, even ? styles.titleEven : {}]}
        numberOfLines={2}
      >
        {`${first_name} ${last_name}`}
      </Text>
    );

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.slideInnerContainer}
        onPress={showDoctorInfo}
      >
        <View style={styles.shadow} />
        <View
          style={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
        >
          {this.image}
          <Avatar
            rounded
            large
            disabled
            source={{
              uri: avatar,
            }}
            activeOpacity={1}
            containerStyle={{
              position: 'absolute',
              top: 10,
              alignSelf: 'center',
            }}
          />
          <View
            style={[styles.radiusMask, even ? styles.radiusMaskEven : {}]}
          />
        </View>
        <View
          style={[styles.textContainer, even ? styles.textContainerEven : {}]}
        >
          {uppercaseTitle}
          <Text
            style={[styles.subtitle, even ? styles.subtitleEven : {}]}
            numberOfLines={2}
          >
            {subtitle}
          </Text>
        </View>
        <CheckBox
          iconName="matEdge"
          style={styles.check}
          onChange={val => {}}
          checkedColor={'white'}
          uncheckedColor={'white'}
        />
      </TouchableOpacity>
    );
  }
}
