import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  ListView,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import { List, ListItem, Avatar } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import CCTextInputAlt from '../../../components/Shared/CCTextInputAlt';
import CCText from '../../../components/Shared/CCText';
import { Actions } from 'react-native-router-flux';
import CCButton from '../../../components/Shared/CCButton';
import config from '../../../../config';
import { styles } from './style';

const RightButtons = props => (
  <View style={{ flexDirection: 'row' }}>
    <TouchableOpacity style={styles.smallButton} onPress={props.handleConsult}>
      <CCText style={styles.smallButtonTitle}>会诊</CCText>
    </TouchableOpacity>
    <TouchableOpacity style={styles.smallButton} onPress={props.handleTreat}>
      <CCText style={styles.smallButtonTitle}>就医</CCText>
    </TouchableOpacity>
  </View>
);

export default class Visit extends Component {
  constructor(props) {
    super(props);
    this.renderRow = this.renderRow.bind(this);
  }

  componentWillMount() {
    this.props.onRefresh();
  }

  onAddressFocus() {
    this.props.onAddressFocus();
  }

  renderRow(rowData, sectionID, rowID) {
    const date = new Date(Date.parse(rowData.creation_date));
    const title =
      rowData.patient_name.trim().length !== 0
        ? rowData.patient_name.trim()
        : rowData.phonenumber;
    const { handleConsult, handleTreat, selectRecord } = this.props;
    return (
      <ListItem
        avatar={
          <Avatar
            rounded
            // source={rowData.avatar_url && { uri: rowData.avatar_url }}
            source={{
              uri:
                'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            }}
            title={rowData.patient_name[0]}
          />
        }
        key={rowID}
        title={title}
        subtitle={date ? date.toDateString() : ' '}
        containerStyle={{ borderBottomWidth: 0 }}
        onPress={() => {
          selectRecord(rowID);
        }}
        rightIcon={
          <RightButtons
            handleConsult={handleConsult}
            handleTreat={handleTreat}
          />
        }
      />
    );
  }

  render() {
    const {
      coordinate,
      currentLocation,
      handleSubmit,
      visitRecords,
      consultRecords,
      treatRecords,
      onRefresh,
    } = this.props;

    const latitude =
      currentLocation.latitude || config.USC_COORDINATES.latitude;

    return (
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={onRefresh} />
        }
        style={styles.wrapper}
      >
        <View style={styles.contentView}>
          <DatePicker
            mode="datetime"
            style={{ width: '100%', marginBottom: 15 }}
          />
          <TouchableOpacity
            onPress={this.onAddressFocus.bind(this)}
            style={styles.input}
            // placeholderTextColor={'rgba(29, 29, 29, 0.45)'}
            // placeholder="地址"
            onFocus={this.onAddressFocus.bind(this)}
          >
            <Text>地址</Text>
          </TouchableOpacity>
          {/* <MapView style={styles.map}> */}
          {/* <Marker
              ref={(marker) => {
                this.currentLocationMarker = marker;
              }}
              coordinate={coordinate}
              anchor={{ x: 0.5, y: 0.5 }}
            >
              <View style={styles.currentMarkerContainer}>
                <View style={styles.currentMarker} />
              </View>
            </Marker> */}
          {/* </MapView> */}
          <CCButton
            color="yellow"
            label="支付"
            handlePress={handleSubmit}
            style={styles.button}
          />
          <CCText style={styles.title}>出诊记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={visitRecords}
            />
          </List>
          <CCText style={styles.title}>会诊记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={consultRecords}
            />
          </List>
          <CCText style={styles.title}>就医记录</CCText>
          <List containerStyle={styles.list}>
            <ListView
              enableEmptySections
              renderRow={this.renderRow}
              dataSource={treatRecords}
            />
          </List>
          {/* <CCButton
            color="yellow"
            label="支付"
            handlePress={handleSubmit}
            style={styles.button}
          /> */}
        </View>
      </ScrollView>
    );
  }
}
