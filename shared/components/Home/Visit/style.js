import { Platform, StyleSheet } from 'react-native';

import { style } from '../../../assets/styles/main';

export const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    backgroundColor: style.WHITE,
  },
  contentView: { padding: 15 },
  list: { borderTopWidth: 0 },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    backgroundColor: style.DARK_GRAY,
    zIndex: 1,
  },
  overlayHidden: {
    display: 'none',
  },
  top: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    width: '100%',
  },
  bottom: {
    width: '100%',
    paddingHorizontal: 15,
    paddingBottom: 15,
  },
  label: {
    fontSize: 20,
    marginBottom: 10,
    color: 'white',
  },
  search: {
    marginBottom: 10,
    position: 'relative',
    zIndex: 1,
  },
  title: {
    marginTop: 30,
    color: style.BLACK,
    fontSize: 15,
  },
  smallButtonTitle: {
    color: style.BLACK,
    fontSize: 15,
  },
  input: {
    // color: style.BLACK,
    height: 45,
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: style.BLACK,
  },
  searchBtn: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  currentMarkerContainer: {
    width: 64,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  currentMarker: {
    width: style.MARKER_WIDTH,
    height: style.MARKER_WIDTH,
    borderRadius: style.MARKER_WIDTH / 2,
    backgroundColor: style.YELLOW,
  },
  button: {
    marginTop: 15,
    marginRight: 40,
    marginLeft: 40,
  },
  smallButton: {
    marginLeft: 5,
    padding: 5,
    backgroundColor: style.YELLOW,
    // borderWidth: 1,
    borderRadius: 15,
    // borderColor: style.SKY,
  },
});
