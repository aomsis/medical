import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { View, ScrollView, TouchableOpacity, Image } from 'react-native';

import {
  loginUser,
  getUser,
  resetPassword,
  resetPasswordForm,
  verifySMS,
  resetVerifyForm,
  resetNewForm,
  setNew,
} from '../../actions/users';
import { isEntireFormElementValid } from '../../utils/formValidator';
import CCText from '../../components/Shared/CCText';
import CCTextInputAlt from '../../components/Shared/CCTextInputAlt';
import CCButton from '../../components/Shared/CCButton';

import styles from './style';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      newpassword: '',
      confirm: '',
      resetError: '',
      loginError: '',
      verifyError: '',
      setNewError: '',
      code: '',
      reset: false,
      setnew: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.cancelReset = this.cancelReset.bind(this);
  }

  componentDidMount() {
    const { auth, currentUser } = this.props;
    if (auth.success) {
      Actions.home();
    }
    return true;
  }

  componentWillReceiveProps(nextProps) {
    const { auth, reset, verify } = nextProps;
    if (auth.success) {
      Actions.home();
    } else if (auth.error) {
      this.setState({
        loginError: auth.errorMessage.message,
      });
    }
    if (verify.success) {
      this.setState({ setnew: true });
    }
    if (reset.error) {
      this.setState({
        resetError: reset.errorMessage.message,
      });
    }
    if (verify.error) {
      this.setState({
        verifyError: verify.errorMessage.message,
      });
    }
    return true;
  }

  get login() {
    return (
      <View style={styles.content}>
        <View>
          <View style={styles.email}>
            <CCTextInputAlt
              style={styles.input}
              placeholder="邮箱"
              keyboardType={'email-address'}
              value={this.state.email}
              handleTextChange={e => this.handleInput(e, 'email')}
            />
          </View>
          <View style={styles.password}>
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry
              placeholder="密码"
              value={this.state.password}
              handleTextChange={e => this.handleInput(e, 'password')}
            />
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.resetPasswordForm();
              this.props.resetVerifyForm();
              this.props.resetNewForm();
              this.setState({ reset: true, setnew: false });
            }}
          >
            <CCText style={styles.reset}>忘记密码</CCText>
          </TouchableOpacity>
          <CCButton
            color="yellow"
            label="登录"
            handlePress={this.handleLogin}
            style={styles.button}
          />
          <CCButton
            color="gray"
            label="注册"
            handlePress={this.handleCreateAccount}
            style={styles.button}
          />
          <CCText style={styles.error}>{this.state.loginError}</CCText>
        </View>
      </View>
    );
  }

  get resetPassword() {
    const { reset, setnew } = this.props;
    let title = '';
    let form;
    let error = '';
    if (this.state.setnew) {
      if (setnew.success) {
        title = 'New password has been set. Please login!';
        form = (
          <CCButton
            color="gray"
            label="回去"
            handlePress={this.cancelReset}
            style={styles.button}
          />
        );
        error = '';
      } else {
        title = 'Please set a new password';
        form = (
          <View>
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry
              placeholder="密码"
              value={this.state.newpassword}
              handleTextChange={e => this.handleInput(e, 'newpassword')}
            />
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry
              placeholder="确认"
              value={this.state.confirm}
              handleTextChange={e => this.handleInput(e, 'confirm')}
            />
          </View>
        );
        error = this.state.setNewError;
      }
    } else if (reset.emailSent) {
      title = 'SMS code has been successfully sent to your mobile device!';
      form = (
        <CCTextInputAlt
          style={styles.input}
          placeholder="Code"
          value={this.state.code}
          handleTextChange={e => this.handleInput(e, 'code')}
        />
      );
      error = this.state.verifyError;
    } else {
      title = 'Enter your email address to request a reset token';
      form = (
        <CCTextInputAlt
          style={styles.input}
          placeholder="邮箱"
          value={this.state.email}
          handleTextChange={e => this.handleInput(e, 'email')}
        />
      );
      error = this.state.resetError;
    }
    return (
      <View style={styles.content}>
        <CCText style={styles.paragraph}>{title}</CCText>
        <View>
          <View style={styles.email}>{form}</View>
          {(!this.state.setnew || !setnew.success) && (
            <View style={styles.btnWrapper}>
              <CCButton
                color="yellow"
                label="下一步"
                handlePress={this.handleReset}
                style={styles.button}
              />
              <CCButton
                color="gray"
                label="取消"
                handlePress={this.cancelReset}
                style={styles.button}
              />
            </View>
          )}
          <CCText style={styles.error}>{error}</CCText>
        </View>
      </View>
    );
  }

  handleInput(value, field) {
    this.setState({ [field]: value, loginError: '', resetError: '' });
  }

  handleLogin() {
    const loginData = {
      email: this.state.email.toLowerCase(),
      password: this.state.password,
    };

    this.props.loginUser(loginData);
  }

  handleCreateAccount() {
    Actions.createAccount();
  }

  handleReset() {
    const { reset } = this.props;
    if (this.state.setnew) {
      if (
        this.state.newpassword === this.state.confirm &&
        this.state.newpassword.length !== 0
      ) {
        const resetData = {
          email: this.state.email.toLowerCase(),
          password: this.state.newpassword,
        };
        this.props.setNew(resetData);
      } else {
        this.setState({ setNewError: 'Passwords do not match' });
      }
    } else if (!reset.emailSent) {
      const resetData = {
        email: this.state.email.toLowerCase(),
      };
      if (isEntireFormElementValid(resetData)) {
        this.props.resetPassword(resetData);
        this.props.resetVerifyForm();
        this.props.resetNewForm();
      } else {
        this.setState({
          resetError: 'You must enter a valid email address',
        });
      }
    } else {
      const resetData = {
        email: this.state.email.toLowerCase(),
        verifycode: this.state.code,
      };
      this.props.verifySMS(resetData);
    }
  }

  cancelReset() {
    this.setState({
      reset: false,
      email: '',
      loginError: '',
      password: '',
      newpassword: '',
      confirm: '',
      code: '',
    });
    this.props.resetPasswordForm();
    this.props.resetVerifyForm();
    this.props.resetNewForm();
  }

  render() {
    return (
      <ScrollView style={styles.login}>
        <View style={styles.loginView}>
          <Image
            style={styles.logo}
            source={require('../../assets/images/cc-white-logo.png')}
          />
          {this.state.reset ? this.resetPassword : this.login}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  currentUser: state.user.currentUser,
  reset: state.user.resetPassword,
  verify: state.user.verifySms,
  setnew: state.user.setNew,
});

export default connect(mapStateToProps, {
  loginUser,
  getUser,
  verifySMS,
  resetPassword,
  resetPasswordForm,
  resetVerifyForm,
  setNew,
  resetNewForm,
})(Login);
