import { StyleSheet, Dimensions } from 'react-native';

import { style } from '../../assets/styles/main';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: style.SKY,
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
  smallButton: {
    marginLeft: Dimensions.get('window').width / 2,
    padding: 5,
    backgroundColor: style.YELLOW,
    // borderWidth: 1,
    borderRadius: 15,
    // borderColor: style.SKY,
  },
  smallButtonTitle: {
    color: style.WHITE,
    fontSize: 15,
    textAlign: 'center',
  },
});

export const { YELLOW } = style;
