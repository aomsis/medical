import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  TouchableOpacity,
  ListView,
  Text,
  ScrollView,
  Image,
} from 'react-native';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
  Cols,
  Cell,
} from 'react-native-table-component';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Video from 'react-native-video';
import { Overlay, Avatar } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Navigation from '../../components/Shared/Navigation';
import Main from '../../components/Home/Main';
import Visit from '../../components/Home/Visit';
import Treat from '../../components/Home/Treat';
import Consult from '../../components/Home/Consult';

import { styles } from './style';
import config from '../../../config';

import { getCurrentPosition } from '../../utils/locationTracking';
import { getMedicalHistory, getDoctors } from '../../actions/rides';
import CCText from '../../components/Shared/CCText/index';

class Home extends Component {
  constructor(props) {
    super(props);

    const dummyVisits = [];
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });

    // create animated region for Marker
    this.state = {
      currentTab: 0,
      coordinate: {
        latitude:
          props.currentLocation.latitude || config.USC_COORDINATES.latitude,
        longitude:
          props.currentLocation.longitude || config.USC_COORDINATES.longitude,
      },
      medicalOverlayVisible: false,
      recordOverlayVisible: false,
      doctorOverlayVisible: false,
      medicalOverlayId: 0,
      doctorOverlayId: 0,
      recordOverlayId: 0,
      visitRecords: ds.cloneWithRows([]),
      consultRecords: ds.cloneWithRows([]),
      treatRecords: ds.cloneWithRows([]),
      tableData: [],
    };

    this.handleConsult = this.handleConsult.bind(this);
    this.handleTreat = this.handleTreat.bind(this);
    this.showDoctor = this.showDoctor.bind(this);
    this.onChangeTab = this.onChangeTab.bind(this);
    this.hideOverlay = this.hideOverlay.bind(this);
    this.selectRecord = this.selectRecord.bind(this);
    this.selectMedicalRecord = this.selectMedicalRecord.bind(this);
  }

  componentWillMount() {
    // GET current location
    getCurrentPosition();
  }

  componentWillReceiveProps(nextProps) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });

    this.state.visitRecords = ds.cloneWithRows(nextProps.medicalHistory);
  }

  onAddressFocus() {
    Actions.addressPicker();
  }

  onChangeTab(tab) {
    this.props.getMedicalHistory();
    if (tab.i === 2) {
      this.props.getDoctors();
    }
    this.setState({ ...this.state, currentTab: tab.i });
  }

  handleVisitPay() {
    Actions.pay();
  }

  handleVisitRefresh() {
    this.props.getMedicalHistory();
  }

  handleConsultPay() {
    Actions.pay();
  }

  handleTreatPay() {
    Actions.pay();
  }

  handleConsult() {
    this.setState({ ...this.state, currentTab: 2 });
  }

  handleTreat() {
    this.setState({ ...this.state, currentTab: 3 });
  }

  hideOverlay() {
    this.setState({
      ...this.state,
      doctorOverlayVisible: false,
      recordOverlayVisible: false,
      medicalOverlayVisible: false,
    });
  }

  showDoctor(index) {
    this.setState({
      ...this.state,
      doctorOverlayVisible: true,
      doctorOverlayId: index,
    });
  }

  selectRecord(rowId) {
    this.setState({
      ...this.state,
      recordOverlayId: rowId,
      recordOverlayVisible: true,
    });
  }

  selectMedicalRecord(rowId) {
    const { medicalHistory } = this.props;
    const medicalRecord = medicalHistory[`${rowId}`];
    const tableData = [];
    if (medicalRecord) {
      // const filtered = Object.keys(medicalRecord)
      //   .filter(key => key !== 'creation_date' && key !== 'patient_name')
      //   .reduce((obj, key) => {
      //     obj[key] = medicalRecord[key];
      //     return obj;
      //   }, {});
      // Object.keys(filtered).forEach(key =>
      //   tableData.push([key, medicalRecord[key]])
      // );
      medicalRecord.excel_data.forEach(value => {
        tableData.push([Object.keys(value)[0], Object.values(value)[0]]);
      });
    }
    this.setState({
      ...this.state,
      medicalOverlayId: rowId,
      // doctorOverlayVisible: false,
      medicalOverlayVisible: true,
      tableData,
    });
  }

  render() {
    const { currentLocation, medicalHistory, doctors } = this.props;

    const {
      coordinate,
      currentTab,
      medicalOverlayId,
      medicalOverlayVisible,
      doctorOverlayVisible,
      recordOverlayVisible,
      doctorOverlayId,
      visitRecords,
      consultRecords,
      treatRecords,
      tableData,
    } = this.state;
    const doctor = doctors[doctorOverlayId];
    const medicalRecord = medicalHistory[`${medicalOverlayId}`];

    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollableTabView
          // locked
          style={{ flex: 1 }}
          page={currentTab}
          onChangeTab={this.onChangeTab}
          tabBarActiveTextColor="white"
          tabBarInactiveTextColor="white"
          tabBarUnderlineStyle={{ backgroundColor: 'white' }}
        >
          <Main tabLabel="首页" />
          <Visit
            tabLabel="面对面初诊"
            coordinate={coordinate}
            currentLocation={currentLocation}
            handleSubmit={this.handleVisitPay.bind(this)}
            handleConsult={this.handleConsult}
            handleTreat={this.handleTreat}
            visitRecords={visitRecords}
            consultRecords={consultRecords}
            treatRecords={treatRecords}
            selectRecord={this.selectMedicalRecord}
            onAddressFocus={this.onAddressFocus}
            onRefresh={this.handleVisitRefresh.bind(this)}
          />
          <Consult
            tabLabel="多人会诊"
            doctors={doctors}
            handleSubmit={this.handleConsultPay}
            handleTreat={this.handleTreat}
            visitRecords={visitRecords}
            consultRecords={consultRecords}
            treatRecords={treatRecords}
            showDoctor={this.showDoctor}
            selectRecord={this.selectMedicalRecord}
          />
          <Treat
            tabLabel="专家就医"
            doctors={doctors}
            handleSubmit={this.handleTreatPay}
            visitRecords={visitRecords}
            consultRecords={consultRecords}
            treatRecords={treatRecords}
            showDoctor={this.showDoctor}
            selectRecord={this.selectMedicalRecord}
          />
        </ScrollableTabView>
        <Navigation />
        {doctorOverlayVisible && (
          <Overlay isVisible={doctorOverlayVisible} width="80%" height="80%">
            <View style={{ flexDirection: 'row' }}>
              <Avatar
                width={120}
                height={120}
                source={{ uri: doctor.avatar }}
                activeOpacity={0.7}
                avatarStyle={{ borderRadius: 60 }}
                overlayContainerStyle={{ backgroundColor: 'transparent' }}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginTop: 10,
                    marginLeft: 10,
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'bold',
                      fontSize: 25,
                      color: 'rgba(98,93,144,1)',
                    }}
                  >
                    {`${doctor.first_name} ${doctor.last_name}`}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                borderColor: 'rgba(222, 223, 226, 1)',
                height: 1,
                marginVertical: 10,
              }}
            />
            <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
              <ScrollView>
                <Text style={{ fontSize: 16 }}>{doctor.description}</Text>
              </ScrollView>
            </View>
            {/* <Button title="OK" onPress={this.hideOverlay} color="#ff0000" /> */}
            <TouchableOpacity
              style={styles.smallButton}
              onPress={this.hideOverlay}
            >
              <CCText style={styles.smallButtonTitle}>OK</CCText>
            </TouchableOpacity>
          </Overlay>
        )}

        {recordOverlayVisible && (
          <Overlay isVisible={recordOverlayVisible} width="80%" height="80%">
            <View style={{ flexDirection: 'row' }}>
              <Avatar
                width={120}
                height={120}
                source={{ uri: doctor.avatar }}
                activeOpacity={0.7}
                avatarStyle={{ borderRadius: 60 }}
                overlayContainerStyle={{ backgroundColor: 'transparent' }}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginTop: 10,
                    marginLeft: 10,
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      fontFamily: 'bold',
                      fontSize: 25,
                      color: 'rgba(98,93,144,1)',
                    }}
                  >
                    {medicalRecord && medicalRecord.name}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                borderColor: 'rgba(222, 223, 226, 1)',
                height: 1,
                marginVertical: 10,
              }}
            />
            <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
              <ScrollView>
                <Text style={{ fontSize: 16 }}>{doctor.desc}</Text>
              </ScrollView>
            </View>
            {/* <Button title="OK" onPress={this.hideOverlay} color="#ff0000" /> */}

            <TouchableOpacity
              style={styles.smallButton}
              onPress={this.hideOverlay}
            >
              <CCText style={styles.smallButtonTitle}>OK</CCText>
            </TouchableOpacity>
          </Overlay>
        )}
        {medicalRecord && (
          <Overlay isVisible={medicalOverlayVisible} width="80%" height="80%">
            <ScrollView style={{ flex: 1, flexDirection: 'column' }}>
              <Text
                style={{
                  fontFamily: 'bold',
                  fontSize: 25,
                  color: 'rgba(98,93,144,1)',
                  marginBottom: 20,
                }}
              >
                记录
              </Text>
              {medicalRecord.image1 && (
                <Avatar
                  width={120}
                  height={120}
                  source={{ uri: `${config.API_URL}/${medicalRecord.image1}` }}
                  activeOpacity={0.7}
                  overlayContainerStyle={{
                    backgroundColor: 'transparent',
                  }}
                  containerStyle={{ alignSelf: 'center', marginVertical: 10 }}
                />
              )}
              <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }}>
                <Rows data={tableData} textStyle={styles.text} />
              </Table>
              {medicalRecord.image2 && (
                <Avatar
                  width={120}
                  height={120}
                  source={{ uri: `${config.API_URL}/${medicalRecord.image2}` }}
                  activeOpacity={0.7}
                  overlayContainerStyle={{
                    backgroundColor: 'transparent',
                  }}
                  containerStyle={{ alignSelf: 'center', marginVertical: 10 }}
                />
              )}
              {medicalRecord.video1 && (
                <Video
                  source={{
                    uri: `${config.API_URL}/${medicalRecord.video1}`,
                  }}
                  resizeMode="cover" // Fill the whole screen at aspect ratio.*
                  style={{ width: 300, height: 300, marginVertical: 10 }}
                  repeat={true}
                />
              )}
            </ScrollView>
            {/* <TouchableOpacity style={{ backgroundColor: 'white', padding: 20, alignItems: 'right' }} onPress={this.hideOverlay}>
            <Text>OK</Text>
          </TouchableOpacity> */}
            <TouchableOpacity
              style={styles.smallButton}
              onPress={this.hideOverlay}
            >
              <CCText style={styles.smallButtonTitle}>OK</CCText>
            </TouchableOpacity>
          </Overlay>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  currentUser: state.user.currentUser,
  currentLocation: state.locations.currentLocation.coords,
  medicalHistory: state.rides.medicalHistory,
  doctors: state.rides.doctors,
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getMedicalHistory,
      getDoctors,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
