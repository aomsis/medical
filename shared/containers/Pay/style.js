import { Platform, StyleSheet } from 'react-native';

import { style } from '../../assets/styles/main';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 35,
    backgroundColor: style.SKY,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  imageContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  image: {
    margin: 20,
    width: 50,
    height: 50,
  },
  money: {
    padding: 10,
    textAlign: 'center',
    fontSize: 30,
  },
});
