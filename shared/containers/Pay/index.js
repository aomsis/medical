import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { View, Image } from 'react-native';

import CCText from '../../components/Shared/CCText';
import CCButton from '../../components/Shared/CCButton';
import CCCloseButton from '../../../shared/components/Shared/CCCloseButton';
import styles from './style';
import { getVisitService } from '../../actions/rides';

class Pay extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    this.props.getVisitService({});
    Actions.pop();
  }

  handleClose() {
    Actions.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <CCCloseButton action={this.handleClose} />
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../../shared/assets/images/alipay.png')}
          />
          <Image
            style={styles.image}
            source={require('../../../shared/assets/images/wechat.png')}
          />
        </View>
        <CCText style={styles.money}>¥30</CCText>
        <CCButton
          color="yellow"
          label="支付"
          handlePress={this.handleSubmit}
          style={styles.button}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getVisitService,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Pay);
