import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import CCText from '../../components/Shared/CCText';
import Navigation from '../../components/Shared/Navigation';
// import AppIntro from './AppIntro';
import { style } from '../../assets/styles/main';

const styles = StyleSheet.create({
  contact: {
    flex: 1,
    backgroundColor: style.SKY,
  },
  scrollView: {
    flex: 1,
  },
  container: {
    padding: 15,
  },
  title: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
    lineHeight: 25,
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
});

export default class Help extends Component {
  render() {
    return (
      <View style={styles.contact}>
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.container}>
            <CCText style={styles.title}>什么是欢聚宝?</CCText>
            <CCText style={styles.text}>
              "欢聚宝"是广州华多网络科技有限公司为方便YY平台的用户进行网上交易推出的安全、稳定、快捷的在线支付平台。{
                '\n'
              }
              为用户提供了多种方便的在线充值、交易管理、在线支付、账户管理等服务。{
                '\n'
              }
              有了"欢聚宝"，您便可以充值到账户，通过余额支付，轻松消费YY平台提供的各种服务，减少了以前每次用银行卡支付的繁琐流程，同时保护了您银行账户信息的安全，让您在YY畅享一站式购物支付体验。{
                '\n'
              }
              有了"欢聚宝"，您还可以享受YY用户专享特惠，购买YY平台上的商品可享受现金券减免或返现。{
                '\n'
              }
              欢聚宝主页：https://pay.yy.com
            </CCText>
          </View>
        </ScrollView>
        <Navigation />
      </View>
    );
  }
}
