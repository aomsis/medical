import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import CCText from '../../components/Shared/CCText';
// import AppIntro from './AppIntro';
import { style } from '../../assets/styles/main';
import Navigation from '../../components/Shared/Navigation';
const styles = StyleSheet.create({
  contact: {
    flex: 1,
    backgroundColor: style.SKY,
  },
  scrollView: {
    flex: 1,
  },
  container: {
    padding: 15,
  },
  title: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
});

export default class TOC extends Component {
  render() {
    return (
      <View style={styles.contact}>
        <Navigation />
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.container}>
            <CCText style={styles.title}>使用条款</CCText>
            <CCText style={styles.text}>
              上次更新：2017年4月1日. 替代之前版本全文。{'\n'}
              这些条款对您的以下行为进行约束：使用我们的网站或Creative{'\n'}
              Cloud之类的服务（下文统称“服务”），以及使用我们作为服务一部分所包含的软件，其中包括所有的应用程序、内容文件（定义见下文）、脚本、指令集，以及任何相关文档（下文统称“软件”）。使用服务或软件即表明您同意这些条款。如果您已就特定服务或软件与我们签订了其他协议，则在其他协议的条款与这些条款发生冲突时，以其他协议的条款为准。正如下面第3条所述，对于您通过服务提供的内容，您可以保留您所拥有的全部权利和所有权。
            </CCText>
          </View>
        </ScrollView>
      </View>
    );
  }
}
