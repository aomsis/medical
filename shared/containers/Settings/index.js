import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Image } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { Dropdown } from 'react-native-material-dropdown';
import CCText from '../../components/Shared/CCText';
import CCTextInputAlt from '../../components/Shared/CCTextInputAlt';
import CCButton from '../../components/Shared/CCButton';
import Navigation from '../../components/Shared/Navigation';
import { updateUser, resetPassword } from '../../actions/users';

import {
  isEntireFormElementValid,
  isPhoneValid,
} from '../../utils/formValidator';
import { formatPhoneNumber } from '../../utils/textStyles';

import { style } from '../../assets/styles/main';
import styles from './style';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phonenumber: '',
      password: 'password',
      error: '',
      firstNameError: '',
      lastNameError: '',
      phoneError: '',
      successMessage: '',
      passwordMessage: '',
    };
    this.data = [{ value: '3' }, { value: '5' }, { value: '10' }];
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleResetPassword = this.handleResetPassword.bind(this);
  }

  componentWillMount() {
    const { currentUser } = this.props;
    this.setState({
      firstName: currentUser.firstName || '',
      lastName: currentUser.lastName || '',
      email: currentUser.email || '',
      phonenumber: currentUser.phonenumber || '',
    });
  }

  componentWillReceiveProps(nextProps) {
    const { currentUser, reset } = nextProps;

    if (currentUser.updateSuccess) {
      this.setState({
        successMessage: 'User Successfully Updated',
      });
    }

    if (reset.emailSent) {
      this.setState({
        passwordMessage: `A password reset link has been emailed to ${
          currentUser.email
        }`,
      });
    } else if (reset.error) {
      this.setState({
        passwordMessage: reset.errorMessage,
      });
    }
  }

  handleInput(value, field) {
    const concatField = `${field}Error`;
    if (field === 'phonenumber') {
      value = value.replace(/\D/g, '');
    }
    this.setState({
      [field]: value,
      [concatField]: '',
      error: '',
      successMessage: '',
      passwordMessage: '',
    });
  }

  handleInputBlur(value, field) {
    const concatField = `${field}Error`;
    const input = this.state[field];
    if (input.length < 1) {
      this.setState({ [concatField]: 'Field must not be empty' });
    }
    if (field === 'phonenumber' && !isPhoneValid(this.state.phonenumber)) {
      this.setState({ [concatField]: 'Phone numbers must be 10 digits' });
    }
    return true;
  }

  handleSubmit() {
    const { currentUser } = this.props;
    const data = {
      id: currentUser.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phonenumber: this.state.phonenumber,
    };
    if (isEntireFormElementValid(data)) {
      this.props.updateUser(data);
    } else {
      this.setState({
        error: 'You must complete all fields',
      });
    }
  }

  handleResetPassword() {
    const { currentUser } = this.props;
    const data = {
      id: currentUser.id,
      email: currentUser.email,
    };
    return this.props.resetPassword(data);
  }

  handleCancel() {
    Actions.home();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: style.BLACK }}>
        <Navigation />
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollView style={styles.settings} keyboardShouldPersistTaps="handled">
          <View style={styles.content}>
            <View style={styles.inputContainer}>
              <CCTextInputAlt
                style={styles.input}
                type="text"
                placeholder="名字"
                keyboardType="default"
                value={this.state.firstName}
                handleTextChange={e => this.handleInput(e, 'firstName')}
                handleInputBlur={e => this.handleInputBlur(e, 'firstName')}
              />
              <CCText style={styles.errorAlt}>
                {this.state.firstNameError}
              </CCText>
            </View>
            <View style={styles.inputContainer}>
              <CCTextInputAlt
                style={styles.input}
                type="text"
                placeholder="姓"
                keyboardType="default"
                value={this.state.lastName}
                handleTextChange={e => this.handleInput(e, 'lastName')}
                handleInputBlur={e => this.handleInputBlur(e, 'lastName')}
              />
              <CCText style={styles.errorAlt}>
                {this.state.lastNameError}
              </CCText>
            </View>
            <View style={styles.inputContainer}>
              <CCTextInputAlt
                style={styles.inputGray}
                type="text"
                placeholder="邮箱"
                keyboardType="email-address"
                value={this.state.email}
                editable={false}
                placeholderTextColor={style.GRAY}
                handleTextChange={e => this.handleInput(e, 'lastName')}
                handleInputBlur={e => this.handleInputBlur(e, 'lastName')}
              />
            </View>
            <View style={styles.inputContainer}>
              <CCTextInputAlt
                style={styles.input}
                type="text"
                placeholder="手机"
                keyboardType="phone-pad"
                maxLength={14}
                value={formatPhoneNumber(this.state.phonenumber)}
                handleTextChange={e => this.handleInput(e, 'phonenumber')}
                handleInputBlur={e => this.handleInputBlur(e, 'phonenumber')}
              />
              <CCText style={styles.errorAlt}>{this.state.phoneError}</CCText>
            </View>
            <View style={styles.inputContainer}>
              <Dropdown
                baseColor="white"
                textColor="white"
                itemColor="rgba(0, 0, 0, 0.4)"
                selectedItemColor="black"
                label="记录个数"
                data={this.data}
              />
            </View>
            {/* <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry={true}
              placeholder="密码"
              editable={false}
              value={this.state.password}
              handleTextChange={(e) => this.handleInput(e, 'password')}
            />
            <TouchableOpacity
              style={styles.reset}
              onPress={this.handleResetPassword}
            >
              <CCText style={styles.resetText}>Reset</CCText>
            </TouchableOpacity>
            <CCText style={[styles.error, styles.passwordError]}>
              {this.state.passwordMessage}
            </CCText>
          </View> */}
          </View>
          <View style={styles.buttonWrapper}>
            <View style={styles.button}>
              <CCButton
                color="yellow"
                label="保存"
                handlePress={this.handleSubmit}
              />
            </View>
            <View style={styles.button}>
              <CCButton
                color="gray"
                label="取消"
                handlePress={this.handleCancel}
              />
            </View>
          </View>
          <CCText style={styles.error}>{this.state.error}</CCText>
          <CCText style={styles.success}>{this.state.successMessage}</CCText>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  currentUser: state.user.currentUser,
  reset: state.user.resetPassword,
});

export default connect(mapStateToProps, { updateUser, resetPassword })(
  Settings
);
