import { Platform, StyleSheet } from 'react-native';

import { style } from '../../assets/styles/main';

export default StyleSheet.create({
  settings: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  button: {
    width: '48%',
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
  buttonWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 35,
  },
  content: {
    width: '100%',
    padding: 35,
    paddingBottom: 15,
    flex: 1,
  },
  error: {
    color: style.RED,
    fontSize: style.FONT_SIZE_M,
    marginTop: 10,
    textAlign: 'center',
  },
  passwordError: {
    height: 40,
    color: style.YELLOW,
  },
  success: {
    textAlign: 'center',
    fontSize: style.FONT_SIZE_M,
  },
  header: {
    fontSize: 40,
    color: style.LIGHTEST_GRAY,
    marginBottom: 15,
  },
  inputContainer: {
    width: '100%',
    marginVertical: 5,
  },
  inputGray: {
    color: style.GRAY,
    borderColor: style.GRAY,
  },
  label: {
    marginBottom: 10,
  },
  paragraph: {
    marginBottom: 30,
    marginTop: 20,
    fontSize: style.FONT_SIZE_XXL,
    color: style.LIGHTEST_GRAY,
  },
  reset: {
    position: 'absolute',
    top: -11,
    right: 0,
    alignSelf: 'flex-end',
  },
  resetText: {
    textAlign: 'center',
    fontSize: style.FONT_SIZE_S,
    color: style.YELLOW,
    padding: 25,
  },
  whiteText: {
    fontSize: style.FONT_SIZE_XXL,
    color: style.WHITE,
  },
  errorAlt: {
    color: style.RED,
    fontSize: style.FONT_SIZE_M,
    height: 20,
  },
});
