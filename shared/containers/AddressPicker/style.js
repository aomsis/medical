import { Platform, StyleSheet } from 'react-native';

import { style } from '../../assets/styles/main';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    backgroundColor: style.SKY,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  input: {
    marginTop: 20,
  },
  map: {
    flex: 1,
    height: 200,
  },
});
