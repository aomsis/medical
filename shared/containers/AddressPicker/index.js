import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { Actions } from 'react-native-router-flux';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Keyboard,
} from 'react-native';
import {
  MapView,
  MapTypes,
  MapModule,
  Geolocation,
} from 'react-native-baidu-map';

import CCText from '../../components/Shared/CCText';
import CCTextInputAlt from '../../components/Shared/CCTextInputAlt';
import CCButton from '../../components/Shared/CCButton';
import CCCloseButton from '../../../shared/components/Shared/CCCloseButton';
import { styles } from './style';

export default class AddressPicker extends Component {
  constructor(props) {
    super(props);
  }

  handleSearch() {
    // Geolocation.geocode
  }

  handleClose() {
    Actions.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <CCCloseButton action={this.handleClose} />
        <CCTextInputAlt
          style={styles.input}
          placeholderTextColor={'rgba(29, 29, 29, 0.45)'}
          placeholder="地址"
        />
        <MapView style={styles.map} />
      </View>
    );
  }
}
