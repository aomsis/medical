import { Platform, StyleSheet } from 'react-native';

import { style } from '../../assets/styles/main';

export default StyleSheet.create({
  login: {
    paddingVertical: 40,
    paddingHorizontal: 35,
    backgroundColor: style.SKY,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  loginView: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
  },
  btnWrapper: {
    marginTop: 35,
  },
  content: {
    width: '100%',
  },
  email: {
    marginVertical: 10,
  },
  error: {
    color: style.RED,
    fontSize: style.FONT_SIZE_S,
    textAlign: 'center',
  },
  header: {
    fontSize: 40,
    color: style.LIGHTEST_GRAY,
    marginBottom: 15,
  },
  label: {
    marginBottom: 10,
  },
  logo: {
    width: 150,
    resizeMode: 'contain',
  },
  password: {
    marginVertical: 10,
    flexDirection: 'row',
  },
  paragraph: {
    marginBottom: 30,
    marginTop: 20,
    fontSize: style.FONT_SIZE_XXL,
    color: style.WHITE,
    textAlign: 'center',
  },
  getCode: {
    textAlign: 'center',
    width: 90,
    fontSize: style.FONT_SIZE_S,
    color: style.WHITE,
  },
  whiteText: {
    fontSize: style.FONT_SIZE_XXL,
    color: style.WHITE,
  },
  button: {
    marginBottom: 15,
  },
});
