import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { View, ScrollView, TouchableOpacity, Image, Text } from 'react-native';

import { loginPhone, verifyPhone } from '../../actions/users';
import CCText from '../../components/Shared/CCText';
import CCTextInputAlt from '../../components/Shared/CCTextInputAlt';
import CCButton from '../../components/Shared/CCButton';

import styles from './style';

class MobileLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      verifycode: '',
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleLoginPhone = this.handleLoginPhone.bind(this);
    this.handleVerifyPhoneLogin = this.handleVerifyPhoneLogin.bind(this);
  }

  componentDidMount() {
    const { authPhone } = this.props;
    if (authPhone.success) {
      Actions.home();
    }
    return true;
  }

  componentWillReceiveProps(nextProps) {
    const { authPhone } = nextProps;
    if (authPhone.success) {
      Actions.home();
    } else if (authPhone.error) {
      this.setState({
        loginError: authPhone.errorMessage.message,
      });
    }
    return true;
  }

  handleInput(value, field) {
    this.setState({ [field]: value, loginError: '' });
  }

  handleLoginPhone() {
    const data = {
      phone: this.state.phone,
    };

    this.props.loginPhone(data);
  }

  handleVerifyPhoneLogin() {
    const data = {
      phone: this.state.phone,
      verifycode: this.state.verifycode,
    };

    this.props.verifyPhone(data);
  }

  render() {
    return (
      <ScrollView style={styles.login}>
        <View style={styles.loginView}>
          <Image
            style={styles.logo}
            source={require('../../assets/images/cc-white-logo.png')}
          />
          <View style={styles.content}>
            <View>
              <View style={styles.email}>
                <CCTextInputAlt
                  style={styles.input}
                  placeholder="手机号"
                  keyboardType={'phone-pad'}
                  value={this.state.phone}
                  handleTextChange={e => this.handleInput(e, 'phone')}
                />
              </View>
              <View style={styles.password}>
                <CCTextInputAlt
                  style={{ flex: 1 }}
                  placeholder="验证码"
                  keyboardType={'phone-pad'}
                  value={this.state.verifycode}
                  handleTextChange={e => this.handleInput(e, 'verifycode')}
                />
                <TouchableOpacity
                  style={{
                    margin: 5,
                    borderRadius: 15,
                    height: 30,
                    backgroundColor: '#393939',
                    justifyContent: 'center',
                  }}
                  onPress={this.handleLoginPhone}
                >
                  <Text style={styles.getCode}>获取验证码</Text>
                </TouchableOpacity>
              </View>

              <CCButton
                color="yellow"
                label="登录"
                handlePress={this.handleVerifyPhoneLogin}
                style={styles.button}
              />
              <CCText style={styles.error}>{this.state.loginError}</CCText>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  authPhone: state.user.authPhone,
});

export default connect(mapStateToProps, {
  loginPhone,
  verifyPhone,
})(MobileLogin);
