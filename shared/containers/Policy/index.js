import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import CCText from '../../components/Shared/CCText';
// import AppIntro from './AppIntro';
import { style } from '../../assets/styles/main';
import Navigation from '../../components/Shared/Navigation';

const styles = StyleSheet.create({
  contact: {
    flex: 1,
    backgroundColor: style.SKY,
  },
  scrollView: {
    flex: 1,
  },
  container: {
    padding: 15,
  },
  title: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
});

export default class Policy extends Component {
  render() {
    return (
      <View style={styles.contact}>
        <Navigation />
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollView style={styles.scrollView}>
          <View style={styles.container}>
            <CCText style={styles.title}>隐私政策</CCText>
            <CCText style={styles.text}>
              腾讯重视用户的隐私。您在使用我们的服务时，我们可能会收集和使用您的相关信息。我们希望通过本《隐私政策》向您说明，在使用我们的服务时，我们如何收集、使用、储存和分享这些信息，以及我们为您提供的访问、更新、控制和保护这些信息的方式。本《隐私政策》与您所使用的腾讯服务息息相关，希望您仔细阅读，在需要时，按照本《隐私政策》的指引，作出您认为适当的选择。本《隐私政策》中涉及的相关技术词汇，我们尽量以简明扼要的表述，并提供进一步说明的链接，以便您的理解。{
                '\n'
              }
              您使用或继续使用我们的服务，即意味着同意我们按照本《隐私政策》收集、使用、储存和分享您的相关信息。
            </CCText>
          </View>
        </ScrollView>
      </View>
    );
  }
}
