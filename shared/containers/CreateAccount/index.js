import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { createUser } from '../../actions/users';
import CCText from '../../components/Shared/CCText';
import CCTextInputAlt from '../../components/Shared/CCTextInputAlt';
import CCButton from '../../components/Shared/CCButton';
import {
  doPasswordsMatch,
  isEmailValid,
  isPasswordValid,
  isEntireFormElementValid,
  isPhoneValid,
} from '../../utils/formValidator';
import { formatPhoneNumber } from '../../utils/textStyles';

import { styles } from './style';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: '',
      passwordCheck: '',
      error: '',
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      phoneError: '',
      passwordError: '',
      passwordCheckError: '',
      newUserSuccess: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleInputBlur = this.handleInputBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    this.setState({ newUserSuccess: false });
  }

  componentWillReceiveProps(nextProps) {
    const { signup } = nextProps;

    if (signup.success) {
      this.setState({ newUserSuccess: true });
    } else if (signup.error) {
      this.setState({
        error: signup.errorMessage,
      });
    }
    return true;
  }

  get inputs() {
    if (!this.state.newUserSuccess) {
      return (
        <View style={styles.content}>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              placeholder="名字"
              keyboardType={'default'}
              value={this.state.firstName}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'firstName')}
              handleInputBlur={e => this.handleInputBlur(e, 'firstName')}
            />
            <CCText style={styles.errorAlt}>{this.state.firstNameError}</CCText>
          </View>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              placeholder="性"
              keyboardType={'default'}
              value={this.state.lastName}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'lastName')}
              handleInputBlur={e => this.handleInputBlur(e, 'lastName')}
            />
            <CCText style={styles.errorAlt}>{this.state.lastNameError}</CCText>
          </View>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              placeholder="邮箱"
              keyboardType={'email-address'}
              value={this.state.email}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'email')}
              handleInputBlur={e => this.handleInputBlur(e, 'email')}
            />
            <CCText style={styles.errorAlt}>{this.state.emailError}</CCText>
          </View>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              placeholder="手机号"
              keyboardType={'phone-pad'}
              maxLength={14}
              value={formatPhoneNumber(this.state.phone)}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'phone')}
              handleInputBlur={e => this.handleInputBlur(e, 'phone')}
            />
            <CCText style={styles.errorAlt}>{this.state.phoneError}</CCText>
          </View>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry={true}
              placeholder="密码"
              keyboardType={'default'}
              value={this.state.password}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'password')}
              handleInputBlur={e => this.handleInputBlur(e, 'password')}
            />
            <CCText style={styles.errorAlt}>{this.state.passwordError}</CCText>
          </View>
          <View style={styles.inputContainer}>
            <CCTextInputAlt
              style={styles.input}
              secureTextEntry={true}
              placeholder="确认密码"
              keyboardType={'default'}
              value={this.state.passwordCheck}
              editable={!this.state.newUserSuccess}
              handleTextChange={e => this.handleInput(e, 'passwordCheck')}
              handleInputBlur={e => this.handleInputBlur(e, 'passwordCheck')}
            />
            <CCText style={styles.errorAlt}>
              {this.state.passwordCheckError}
            </CCText>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }

  get buttons() {
    if (this.state.newUserSuccess) {
      return (
        <View style={styles.backWrapper}>
          <CCButton
            color="yellow"
            label="回去"
            handlePress={this.handleCancel}
          />
        </View>
      );
    }
    return (
      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <CCButton
            color="yellow"
            label="保存"
            handlePress={this.handleSubmit}
          />
        </View>
        <View style={styles.button}>
          <CCButton color="gray" label="取消" handlePress={this.handleCancel} />
        </View>
      </View>
    );
  }

  get header() {
    if (this.state.newUserSuccess) {
      return <CCText style={styles.paragraph}>Success! Please login.</CCText>;
    }
    return (
      <CCText style={styles.paragraph}>
        Please enter your information to create an account.
      </CCText>
    );
  }

  handleInput(value, field) {
    let val = value;
    const concatField = `${field}Error`;
    if (field === 'phone') {
      val = value.replace(/\D/g, '');
    }
    this.setState({
      [field]: val,
      error: '',
      [concatField]: '',
    });
  }

  handleInputBlur(e, field) {
    const concatField = `${field}Error`;
    const value = this.state[field];
    if (value.length < 1) {
      this.setState({ [concatField]: 'Field must not be empty' });
    }
    if (
      (field === 'password' && !isPasswordValid(value)) ||
      (field === 'passwordCheck' && !isPasswordValid(value))
    ) {
      this.setState({
        [concatField]:
          'Passwords must be 7 characters long and contain at least 1 number, one uppercase letter, and one lowercase letter',
      });
    }
    if (field === 'phone' && !isPhoneValid(this.state.phone)) {
      this.setState({
        [concatField]: 'Phone number must be a valid 10 digit number',
      });
    }
    return true;
  }

  handleSubmit() {
    const data = {
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      email: this.state.email.toLowerCase(),
      phonenumber: this.state.phone,
      password: this.state.password,
    };

    if (
      doPasswordsMatch(this.state.password, this.state.passwordCheck) &&
      isEntireFormElementValid(data)
    ) {
      return this.props.createUser(data);
    } else if (
      !doPasswordsMatch(this.state.password, this.state.passwordCheck)
    ) {
      this.setState({
        error: 'Passwords do not match',
      });
      // }
      // else if (
      //   !isPasswordValid(data.password) ||
      //   !isPasswordValid(data.passwordCheck)
      // ) {
      //   this.setState({
      //     error:
      //       'Passwords must be 7 characters long and contain at least 1 number, one uppercase letter, and one lowercase letter',
      //   });
    } else if (!isEmailValid(data.email)) {
      this.setState({
        error: 'You must use email address',
      });
    } else if (!isPhoneValid(data.phonenumber)) {
      this.setState({
        error: 'Phone number is invalid',
      });
    } else if (!isEntireFormElementValid(data)) {
      this.setState({
        error: 'You must complete all fields',
      });
    }
  }

  handleCancel() {
    Actions.pop();
  }

  render() {
    return (
      <ScrollView
        style={styles.createAccount}
        keyboardShouldPersistTaps="handled"
      >
        <View style={styles.createAccountView}>
          <Image
            style={styles.logo}
            source={require('../../assets/images/cc-white-logo.png')}
          />
          {this.header}
          <View style={styles.content}>
            {this.inputs}
            {this.buttons}
            <CCText style={styles.error}>{this.state.error}</CCText>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user.auth,
  currentUser: state.user.currentUser,
  signup: state.user.signup,
});

export default connect(mapStateToProps, { createUser })(Login);
