import { Platform, StyleSheet } from 'react-native';

import { style } from '../../assets/styles/main';

export const styles = StyleSheet.create({
  createAccount: {
    height: '100%',
    backgroundColor: style.SKY,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  createAccountView: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 40,
    paddingHorizontal: 20,
  },
  button: {
    width: '48%',
  },
  buttonWrapper: {
    width: '100%',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  content: {
    width: '100%',
  },
  error: {
    color: style.RED,
    fontSize: style.FONT_SIZE_S,
    marginTop: 10,
    textAlign: 'center',
  },
  errorAlt: {
    color: style.RED,
    fontSize: style.FONT_SIZE_XS,
  },
  header: {
    fontSize: 40,
    color: style.LIGHTEST_GRAY,
    marginBottom: 15,
  },
  inputContainer: {
    width: '100%',
  },
  label: {
    marginBottom: 10,
  },
  logo: {
    width: 150,
    resizeMode: 'contain',
  },
  nameWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paragraph: {
    marginVertical: 20,
    fontSize: style.FONT_SIZE_XXL,
    color: style.WHITE,
    textAlign: 'center',
  },
  reset: {
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 55,
    fontSize: style.FONT_SIZE_S,
    color: style.LIGHTEST_GRAY,
    textDecorationLine: 'underline',
    alignSelf: 'flex-end',
  },
  whiteText: {
    fontSize: style.FONT_SIZE_XXL,
    color: style.WHITE,
  },
});
