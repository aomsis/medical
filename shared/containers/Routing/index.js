import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

class Routing extends Component {
  componentWillMount() {
    const { currentUser } = this.props;

    if (!currentUser.success) {
      Actions.walkthrough({ type: 'reset' });
    } else {
      Actions.home({ type: 'reset' });
    }
  }

  render() {
    return <View />;
  }
}

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
});

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Routing);
