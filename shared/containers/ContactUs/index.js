import React, { Component } from 'react';
import { StyleSheet, View, Image, ScrollView } from 'react-native';
import CCText from '../../components/Shared/CCText';
import { style } from '../../assets/styles/main';
import Navigation from '../../components/Shared/Navigation';

const styles = StyleSheet.create({
  contact: {
    flex: 1,
  },
  scrollContainer: {
    padding: 20,
  },
  container: {
    flex: 1,
    backgroundColor: style.SKY,
  },
  map: {
    alignSelf: 'center',
    width: '80%',
    marginBottom: 20,
  },
  text: {
    marginTop: 5,
    marginBottom: 5,
    fontSize: 18,
  },
  logo: {
    alignSelf: 'center',
    width: 150,
    resizeMode: 'contain',
  },
});

export default class ContactUs extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Navigation />
        <Image
          style={styles.logo}
          source={require('../../assets/images/cc-white-logo.png')}
        />
        <ScrollView style={styles.contact}>
          <View style={styles.scrollContainer}>
            <Image
              style={styles.map}
              source={require('../../assets/images/contact-map.png')}
            />
            <CCText style={styles.text}>北京比邻弘科科技有限公司</CCText>
            <CCText style={styles.text}>
              地址： 北京市朝阳区朝阳北路103号金泰国益大厦1208室
            </CCText>
            <CCText style={styles.text}>电话： 010-57384016</CCText>
            <CCText style={styles.text}>北京比邻弘科科技有限公司</CCText>
            <CCText style={styles.text}>邮箱： operation@lbadvisor.net</CCText>
            <CCText style={styles.text}>
              量江湖官网： http://liangjianghu.com/
            </CCText>
            <CCText style={styles.text}>微信公众号： adwujialiang</CCText>
          </View>
        </ScrollView>
      </View>
    );
  }
}
