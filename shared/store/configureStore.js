import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import rootReducer from '../reducers/index';
import middlewares from '../middlewares';

/*
 * configuration for persistConfig
 */
const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
};

const enhancer = compose(applyMiddleware(...middlewares));

const reducers = persistReducer(persistConfig, rootReducer);

export const store = createStore(reducers, enhancer);
export const persistor = persistStore(store);
