import { Linking } from 'react-native';

export default class Linker {
  static openExternal(data) {
    if (data.type == 0) {
      // Open direction with Google Maps or Apple Map app
      // data.location
      Linking.canOpenURL('comgooglemaps://').then(supported => {
        if (!supported) {
          return Linking.openURL(`http://maps.apple.com/?daddr=${data.location.lat},${data.location.lng}&dirflg=driving`)
        } else {
          return Linking.openURL(`comgooglemaps://?daddr=${data.location.lat},${data.location.lng}&directionsmode=driving`);
        }
      }).catch(err => console.error('An error occurred', err));
    } else {
      // Open browser link
      // data.url
      Linking.openURL(data.url);
    }
  }
}
