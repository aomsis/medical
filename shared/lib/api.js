'use strict';

import { AsyncStorage } from 'react-native';

// Connect this URL to staging
import config from '../../config';

class Api {
  static headers(route) {
    return AsyncStorage.getItem('cc-token').then(token => {
      console.log('------');
      console.log(token);
      return Promise.resolve({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        dataType: 'json',
        Authorization: `${token}`,
      });
    });
  }

  static get(route) {
    return this.xhr(route, null, 'GET');
  }

  static put(route, params) {
    return this.xhr(route, params, 'PUT');
  }

  static post(route, params) {
    return this.xhr(route, params, 'POST');
  }

  static delete(route, params) {
    return this.xhr(route, params, 'DELETE');
  }

  static xhr(route, params, verb) {
    const url = `${config.API_URL}${route}`;
    let options = Object.assign(
      { method: verb },
      params ? { body: JSON.stringify(params) } : null
    );
    return Api.headers(route).then(headers => {
      options.headers = headers;
      return fetch(url, options)
        .then(resp => {
          let json = resp.json();
          if (resp.ok) {
            return json;
          }
          return json.then(err => {
            throw err;
          });
        })
        .then(json => {
          return json;
        });
    });
  }
}

export default Api;
