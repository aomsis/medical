export const initialLocation = {
  coords: {
    altitudeAccuracy: 0,
    accuracy: 0,
    heading: 0,
    longitude: 0,
    altitude: 0,
    latitude: 0,
    speed: 0,
  },
  timestamp: 0,
};
