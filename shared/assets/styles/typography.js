const FONT_OPEN_SANS = 'OpenSans-Regular';
const FONT_OPEN_SANS_BOLD = 'OpenSans-Bold';
const FONT_SIZE_XXS = 8;
const FONT_SIZE_XS = 10;
const FONT_SIZE_S = 12;
const FONT_SIZE_M = 14;
const FONT_SIZE_L = 15;
const FONT_SIZE_XL = 16;
const FONT_SIZE_XXL = 18;
const FONT_SIZE_XXXL = 24;

export const fonts = {
  FONT_OPEN_SANS
};

export const fontSizes = {
  FONT_SIZE_XXS,
  FONT_SIZE_XS,
  FONT_SIZE_S,
  FONT_SIZE_M,
  FONT_SIZE_L,
  FONT_SIZE_XL,
  FONT_SIZE_XXL,
  FONT_SIZE_XXXL
}
