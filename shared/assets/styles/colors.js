const BLACK = '#1d1d1d';
const WHITE = '#FFFFFF';
const GRAY = '#393939';
const DARK_GRAY = '#252525';
const LIGHT_GRAY = '#949494';
const LIGHTER_GRAY = '#a5a5a5';
const LIGHTEST_GRAY = '#bfbfbf';
const RED = '#990000';
const LIGHT_RED = '#ad0000';
const YELLOW = '#fecd08';
const TRANSPARENT_BLACK = 'rgba(29, 29, 29, 0.45)';
const TRANSPARENT_BLACK_DARK = 'rgba(29, 29, 29, 0.95)';
const TRANSPARENT_WHITE = 'rgba(255, 255, 255, 0.25)';
const MAP_BLACK = '#282828';
const SKY = '#0099df';

export const colors = {
  BLACK,
  WHITE,
  GRAY,
  DARK_GRAY,
  LIGHT_GRAY,
  LIGHTER_GRAY,
  LIGHTEST_GRAY,
  RED,
  LIGHT_RED,
  YELLOW,
  TRANSPARENT_BLACK,
  TRANSPARENT_BLACK_DARK,
  TRANSPARENT_WHITE,
  MAP_BLACK,
  SKY
};
