import { assign } from 'lodash';

import { colors } from './colors';
import { fonts, fontSizes } from './typography';
import { markers } from './markers';

export const style = assign(colors, fonts, fontSizes, markers);
