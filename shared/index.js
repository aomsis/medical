// FIX: get rid of the self error
// if (typeof global.self === 'undefined') {
//   global.self = global;
// }

import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { PersistGate } from 'redux-persist/lib/integration/react';

import { store, persistor } from './store/configureStore';

import Routing from './containers/Routing';
import Login from './containers/Login';
import MobileLogin from './containers/MobileLogin';
import CreateAccount from './containers/CreateAccount';
import Home from './containers/Home';
import Settings from './containers/Settings';
import Pay from './containers/Pay';
import ContactUs from './containers/ContactUs';
import Help from './containers/Help';
import TOC from './containers/TOC';
import Policy from './containers/Policy';
import Walkthrough from './containers/AppIntro';
import LoadingView from './components/LoadingView';
import AddressPicker from './containers/AddressPicker';

import { requestLocation, clearPositionWatch } from './utils/locationTracking';

const RouterWithRedux = connect()(Router);

// add scene configuration
const sceneConfig = {
  cardStyle: {
    backgroundColor: '#000',
  },
};

export default class App extends Component {
  componentDidMount() {
    // request for location permission
    // setTimeout here to FIX the bug with mounting before Activity is loaded
    setTimeout(() => requestLocation(this.watchId), 0);
    setTimeout(() => SplashScreen.hide(), 2000);
  }

  componentWillUnmount() {
    // Remove position watch
    clearPositionWatch(this.watchId);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<LoadingView />} persistor={persistor}>
          <RouterWithRedux>
            <Scene key="root" {...sceneConfig}>
              <Scene key="routing" component={Routing} hideNavBar initial />
              <Scene key="login" component={Login} title="Login" hideNavBar />
              <Scene
                key="mobileLogin"
                component={MobileLogin}
                title="Login"
                hideNavBar
              />
              <Scene
                key="createAccount"
                component={CreateAccount}
                title="CreateAccount"
                hideNavBar
              />
              <Scene
                key="home"
                component={Home}
                title="Home"
                hideNavBar
                type="reset"
              />
              <Scene
                key="addressPicker"
                component={AddressPicker}
                title="AddressPicker"
                hideNavBar
              />
              <Scene
                key="settings"
                component={Settings}
                title="Settings"
                hideNavBar
              />
              <Scene
                key="walkthrough"
                component={Walkthrough}
                title="Settings"
                hideNavBar
              />
              <Scene
                key="contact"
                component={ContactUs}
                title="Contact"
                hideNavBar
              />
              <Scene key="help" component={Help} title="Help" hideNavBar />
              <Scene
                key="policy"
                component={Policy}
                title="Policy"
                hideNavBar
              />
              <Scene key="toc" component={TOC} title="TOC" hideNavBar />
              <Scene
                key="settings"
                component={Settings}
                title="Settings"
                hideNavBar
              />
              <Scene key="pay" component={Pay} title="Pay" hideNavBar />
            </Scene>
          </RouterWithRedux>
        </PersistGate>
      </Provider>
    );
  }
}
