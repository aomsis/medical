'use strict';

/*
 * SWITCH CONDITIONS THAT NEED TO BE MODULARIZED
 */

export function selectLocationIcon(type, color) {
  const icons = {
    residential: {
      yellow: require(`../assets/images/residence-icon-yellow.png`),
      white: require(`../assets/images/residence-icon-white.png`)
    },
    university: {
      yellow: require(`../assets/images/college-icon-yellow.png`),
      white: require(`../assets/images/college-icon-white.png`)
    }
  }

  if (!type || type !== 'residential') return icons['university'][color];
  return icons[type][color];
}
