'use strict';

/*
 * Title Case a string
 * @params str: String
 */
export function toTitleCase(str) {
  // validation
  if (typeof str !== 'string') return '';

  // function
  return str.replace(/\w\S*/g, function(txt) {
    const text = txt.toLowerCase();

    // handle Mc cases
    if (text.substring(0, 2) === 'mc') {
      return 'Mc' + text.charAt(2).toUpperCase() + text.substr(3);
    }

    // handle one-off cases and default case
    switch (text) {
      case 'lac':
      case 'ca':
      case 'usc':
        return text.toUpperCase();
      default:
        return text.charAt(0).toUpperCase() + text.substr(1);
    }
  });
}

export function formatPhoneNumber(input) {
  // remove non numbers
  input = input.toString();
  input = input.replace(/\D/g, '');

  // limit to 10 chars
  input = input.substring(0, 11);

  var size = input.length;
  if (size < 4) {
    input = input;
  } else if (size < 8) {
    input = input.substring(0, 3) + '-' + input.substring(3, 7);
  } else {
    input =
      input.substring(0, 3) +
      '-' +
      input.substring(3, 7) +
      '-' +
      input.substring(7, 11);
  }
  return input;
}

export function formatStreetName(str) {
  // validation
  if (typeof str !== 'string') return '';

  // function
  const string = str.toLowerCase();
  const idx = string.indexOf('los angeles');
  let street = str.substring(0, idx);
  street = street.replace(/\,/g, '');
  street = street.trim();
  return street;
}

export function truncateName(str) {
  // validation
  if (typeof str !== 'string') return '';

  const maxLength = 35;

  if (str.length < maxLength) return str;

  let trimmedString = str.substr(0, maxLength - 3);
  trimmedString = trimmedString.substr(
    0,
    Math.min(trimmedString.length, trimmedString.lastIndexOf(' '))
  );
  return `${trimmedString}...`;
}
