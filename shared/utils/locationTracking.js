'use strict';

/*
 * Grab the store to dispatch with
 */

import { Platform, Alert, Linking } from 'react-native';
import { store } from '../store/configureStore';
import { setLocation, getStartingLocation } from '../actions/locations';
import Permissions from 'react-native-permissions';

/*
 * Request permission on Android
 */

function checkLocationPermission() {
  return Permissions.check('location');
}

function getLocationPermission(permission) {
  if (permission !== 'authorized' || permission === 'denied') {
    return Permissions.request('location');
  }
  return Promise.resolve(permission);
}

export const requestLocation = async watchId => {
  let permission;

  await checkLocationPermission();

  permission = await getLocationPermission();

  if (permission === 'authorized') {
    await getCurrentPosition();
    await watchCurrentPosition(watchId);
    return true;
  } else {
    await store.dispatch(clearFromLocation());
    Alert.alert(
      'Location tracking is required for Campus Cruiser.',
      'Please enable location tracking in your phone settings',
      [
        { text: 'Cancel', style: 'cancel' },
        { text: 'Settings', onPress: () => Linking.openURL('app-settings:') },
      ],
      { cancelable: true }
    );
    return false;
  }
};

/*
 * - Find the current location
 */
export function getCurrentPosition() {
  navigator.geolocation.getCurrentPosition(
    position => {},
    error => {
      console.log('error', error.message);
    }
  );
}

/*
 * Watch for position changes
 */
export function watchCurrentPosition(watchId) {
  watchId = navigator.geolocation.watchPosition(
    position => {
      // Save the location to reducer
      store.dispatch(setLocation(position));
    },
    error => {
      console.log('error', error);
    },
    {
      enableHighAccuracy: true,
      timeout: 20000,
      distanceFilter: 10,
    }
  );
}

/*
 * Clear the watch
 */
export function clearPositionWatch(watchId) {
  console.log('stop watching position');
  navigator.geolocation.clearWatch(watchId);
}
