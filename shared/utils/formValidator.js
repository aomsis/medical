import { forEach } from 'lodash';

function isEmailValid(email) {
  // uncommit for live production
  // const re = /^[A-Za-z0-9._%+-]+@usc.edu$/
  // return re.test(email);
  return true;
}

function isPhoneValid(phone) {
  return phone.length === 11;
}

function isPasswordValid(password) {
  const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/;
  return re.test(password) && password.length >= 7;
}

function isEntireFormElementValid(data) {
  let formIsValid = true;
  forEach(data, (value, key) => {
    if (!value) {
      formIsValid = false;
    }

    if (key === 'email') {
      if (!isEmailValid(value)) {
        // only run regex validator on field if there is input and it's required
        formIsValid = false;
      }
    }

    if (key === 'password' || key === 'passwordCheck') {
      if (!isPasswordValid(value)) {
        formIsValid = false;
      }
    }
  });
  return formIsValid;
}

function doPasswordsMatch(field1, field2) {
  return field1 === field2;
}

export {
  isEmailValid,
  isEntireFormElementValid,
  doPasswordsMatch,
  isPasswordValid,
  isPhoneValid,
};
