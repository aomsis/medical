export const filterLocations = (list, location) => {
  /*
   * Filter
   * - when the item is the same location as the starting location
   * - when the campus is not the same as the starting location
   */
  let newLocations;
  if (!!location.id) {
    newLocations = list.filter(item => {
      return item.id !== location.id && item.campus === location.campus;
    });
    if (newLocations.length > 5) {
      return newLocations.slice(0, 5);
    }
    return newLocations;
  } else {
    return list;
  }
};
