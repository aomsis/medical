import { assign } from 'lodash';
import { combineReducers } from 'redux';
import types from '../types/locations';
import { initialLocation } from '../initialStates/locations';

export const currentLocation = (state = initialLocation, action) => {
  switch (action.type) {
    case types.SET_LOCATION:
      return assign(state, action.location);
    default:
      return state;
  }
};

export default combineReducers({
  currentLocation,
});
