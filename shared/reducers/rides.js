import { Alert } from 'react-native';
import { assign } from 'lodash';
import { combineReducers } from 'redux';
import types from '../types/rides';
import config from '../../config';

export const visitServiceComplete = (state = false, action) => {
  switch (action.type) {
    case `${types.GET_VISIT_SERVICE}_SUCCESS`:
      return true;
    case `${types.GET_VISIT_SERVICE}_FAILURE`:
      return false;
    default:
      return state;
  }
};

export const medicalHistory = (state = [], action) => {
  switch (action.type) {
    case `${types.GET_MEDICAL_HISTORY}_SUCCESS`:
      return assign({}, action.json.data);
    case `${types.GET_MEDICAL_HISTORY}_FAILURE`:
      return [];
    default:
      return state;
  }
};

export const doctors = (state = [], action) => {
  switch (action.type) {
    case `${types.GET_DOCTORS}_SUCCESS`: {
      const results = action.json.data.map(doctor => ({
        ...doctor,
        avatar: `${config.API_URL}/${doctor.avatar}`,
      }));
      return assign([], results);
    }
    case `${types.GET_DOCTORS}_FAILURE`:
      return [];
    default:
      return state;
  }
};

export default combineReducers({
  visitServiceComplete,
  medicalHistory,
  doctors,
});
