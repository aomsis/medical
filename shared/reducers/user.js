'use strict';
import { AsyncStorage } from 'react-native';
import { combineReducers } from 'redux';
import * as types from '../types/users';
import { assign } from 'lodash';

const currentUser = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
    updateSuccess: false,
  },
  action
) => {
  switch (action.type) {
    case `${types.CREATE_USER}_REQUEST`:
    case `${types.UPDATE_USER}_REQUEST`:
    case `${types.UPDATE_DEVICE_TOKEN}_REQUEST`:
    case `${types.RESET_PASSWORD}_REQUEST`:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
        updateSuccess: false,
      });
    case `${types.GET_USER}_SUCCESS`:
    case `${types.LOGIN}_SUCCESS`:
    case `${types.VERIFY_PHONE}_SUCCESS`:
      console.log(action.json);
      AsyncStorage.setItem('cc-token', action.json.token);
      return assign({}, action.json.data, {
        token: action.json.token,
        success: true,
      });
    case `${types.UPDATE_USER}_SUCCESS`:
      return assign({}, action.json, { updateSuccess: true });
    case `${types.CREATE_USER}_SUCCESS`:
    case `${types.UPDATE_DEVICE_TOKEN}_SUCCESS`:
      return assign({}, action.json, { success: true });
    case `${types.CREATE_USER}_FAILURE`:
    case `${types.GET_USER}_FAILURE`:
    case `${types.LOGIN}_FAILURE`:
    case `${types.UPDATE_USER}_FAILURE`:
    case `${types.UPDATE_DEVICE_TOKEN}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const signup = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.CREATE_USER}_REQUEST`:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    case `${types.CREATE_USER}_SUCCESS`:
      return assign({}, action.json, { success: true });
    case `${types.CREATE_USER}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const auth = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.LOGIN}_REQUEST`:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    case `${types.LOGIN}_SUCCESS`:
      return assign({}, ...action.json, { success: true });
    case `${types.LOGIN}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    case `${types.LOGOUT}_SUCCESS`:
      AsyncStorage.removeItem('cc-token');
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    default:
      return state;
  }
};

const authPhone = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.VERIFY_PHONE}_REQUEST`:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    case `${types.VERIFY_PHONE}_SUCCESS`:
      return assign({}, ...action.json, { success: true });
    case `${types.VERIFY_PHONE}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const currentPage = (
  state = {
    activePage: 'profile',
    index: 0,
  },
  action
) => {
  switch (action.type) {
    case types.UPDATE_PAGE_INDEX:
      return assign({}, action.data);
    default:
      return state;
  }
};

const resetPassword = (
  state = {
    emailSent: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.UPDATE_USER}_REQUEST`:
    case `${types.RESET_PASSWORD}_REQUEST`:
    case types.RESET_PASSWORD_FORM:
      return assign({}, state, {
        emailSent: false,
        error: false,
        errorMessage: '',
      });
    case `${types.RESET_PASSWORD}_SUCCESS`:
      return assign({}, ...action.json, { emailSent: true });
    case `${types.RESET_PASSWORD}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const verifySms = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.VERIFY_SMS}_REQUEST`:
    case types.RESET_VERIFY_FORM:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    case `${types.VERIFY_SMS}_SUCCESS`:
      return assign({}, ...action.json, { success: true });
    case `${types.VERIFY_SMS}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const setNew = (
  state = {
    success: false,
    error: false,
    errorMessage: '',
  },
  action
) => {
  switch (action.type) {
    case `${types.SET_NEW}_REQUEST`:
    case types.RESET_NEWSET_FORM:
      return assign({}, state, {
        success: false,
        error: false,
        errorMessage: '',
      });
    case `${types.SET_NEW}_SUCCESS`:
      return assign({}, ...action.json, { success: true });
    case `${types.SET_NEW}_FAILURE`:
      return assign({}, state, {
        error: true,
        errorMessage: action.errorMessage,
      });
    default:
      return state;
  }
};

const blankForm = {
  nickname: '',
};

const userForm = (state = blankForm, action) => {
  switch (action.type) {
    case types.RESET_USER_FORM:
      return blankForm;
    case types.UPDATE_USER_FORM:
      return { ...state, ...action.data };
    case `${types.UPDATE_USER}_SUCCESS`:
    case `${types.CREATE_USER}_SUCCESS`:
      return { ...blankForm, isUpdated: true };
    default:
      return state;
  }
};

export default combineReducers({
  currentUser,
  auth,
  authPhone,
  signup,
  userForm,
  resetPassword,
  verifySms,
  currentPage,
  setNew,
});
