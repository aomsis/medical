import { combineReducers } from 'redux';
import user from './user';
import locations from './locations';
import rides from './rides';

import types from '../types/users';

const appReducer = combineReducers({
  user,
  locations,
  rides,
});

const rootReducer = (state, action) => {
  /*
   * WIPE OUT THE STATE ON LOGOUT
   */

  if (action.type === `${types.LOGOUT}_SUCCESS`) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
