'use strict';

import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import promiseMiddleware from './promise.js';

const loggerMiddleware = createLogger({
  predicate: (getState, action) => __DEV__
});

const middlewares = [
  thunk,
  promiseMiddleware,
  loggerMiddleware,
];

export default middlewares;
