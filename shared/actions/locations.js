'use strict';

import types from '../types/locations';

/*
 * @params location: object
 */
export const setLocation = location => ({
  type: types.SET_LOCATION,
  location,
});
