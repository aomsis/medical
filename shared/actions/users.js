'use strict';

import * as types from '../types/users.js';
import Api from '../lib/api';

export const createUser = data => ({
  type: types.CREATE_USER,
  promise: Api.post('/register/', data),
});

export const getUser = () => ({
  type: types.GET_USER,
  promise: Api.get('/me'),
});

export const updateUser = data => ({
  type: types.UPDATE_USER,
  promise: Api.put(`/passenger/${data.id}`, data),
});

export const loginUser = data => ({
  type: types.LOGIN,
  promise: Api.post('/login/', data),
});

export const loginPhone = data => ({
  type: types.LOGIN_PHONE,
  promise: Api.post('/loginphone/', data),
});

export const verifyPhone = data => ({
  type: types.VERIFY_PHONE,
  promise: Api.post('/verifyphonelogin/', data),
});

export const logout = () => ({
  // type: types.LOGOUT,
  // promise: Api.delete('/logout')
  type: `${types.LOGOUT}_SUCCESS`,
});

export const resetPassword = data => ({
  type: types.RESET_PASSWORD,
  promise: Api.post('/forgotpass/', data),
});

export const verifySMS = data => ({
  type: types.VERIFY_SMS,
  promise: Api.post('/verifycode/', data),
});

export const setNew = data => ({
  type: types.SET_NEW,
  promise: Api.post('/setpass/', data),
});

export const updateUserForm = data => ({
  type: types.UPDATE_USER_FORM,
  data,
});

export const resetPasswordForm = data => ({
  type: types.RESET_PASSWORD_FORM,
  data,
});

export const resetVerifyForm = () => ({
  type: types.RESET_VERIFY_FORM,
});

export const resetNewForm = () => ({
  type: types.RESET_NEWSET_FORM,
});

export const updatePageIndex = data => ({
  type: types.UPDATE_PAGE_INDEX,
  data,
});

export const resetUserForm = () => ({
  type: types.RESET_USER_FORM,
});

export const setDeviceToken = deviceToken => ({
  type: types.SET_DEVICE_TOKEN,
  deviceToken,
});

export const updateDeviceToken = (userId, deviceToken) => ({
  type: types.UPDATE_DEVICE_TOKEN,
  promise: Api.put(`/passenger/${userId}`, { deviceToken }),
});
