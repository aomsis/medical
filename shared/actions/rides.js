import types from '../types/rides';
import Api from '../lib/api';

export const getVisitService = data => ({
  type: types.GET_VISIT_SERVICE,
  promise: Api.xhr('/chujonservice/', data, 'POST'),
});

export const getMedicalHistory = () => ({
  type: types.GET_MEDICAL_HISTORY,
  promise: Api.xhr('/medicalhistory/', null, 'GET'),
});

export const getDoctors = () => ({
  type: types.GET_DOCTORS,
  promise: Api.xhr('/doctors/', null, 'GET'),
});
