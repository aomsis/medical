module.exports = {
  CREATE_USER: 'CREATE_USER',

  GET_USER: 'GET_USER',

  UPDATE_USER: 'UPDATE_USER',

  LOGIN: 'LOGIN',

  LOGIN_PHONE: 'LOGIN_PHONE',
  VERIFY_PHONE: 'VERIFY_PHONE',

  LOGOUT: 'LOGOUT',

  RESET_PASSWORD: 'RESET_PASSWORD',

  VERIFY_SMS: 'VERIFY_SMS',

  SET_NEW: 'SET_NEW',

  RESET_PASSWORD_FORM: 'RESET_PASSWORD_FORM',

  RESET_VERIFY_FORM: 'RESET_VERIFY_FORM',

  RESET_NEWSET_FORM: 'RESET_NEWSET_FORM',

  UPDATE_USER_FORM: 'UPDATE_USER_FORM',

  RESET_USER_FORM: 'RESET_USER_FORM',

  UPDATE_PAGE_INDEX: 'UPDATE_PAGE_INDEX',

  UPDATE_DEVICE_TOKEN: 'UPDATE_DEVICE_TOKEN',
};
