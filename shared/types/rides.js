import keyMirror from 'keymirror';

const types = keyMirror({
  GET_VISIT_SERVICE: null,
  GET_MEDICAL_HISTORY: null,
  GET_DOCTORS: null,
});

export default types;
