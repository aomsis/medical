import keyMirror from 'keymirror';

const types = keyMirror({
  SET_LOCATION: null,
});

export default types;
