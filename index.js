'use strict';

import { AppRegistry } from 'react-native';
import App from './shared';

AppRegistry.registerComponent('campus_cruiser', () => App);
