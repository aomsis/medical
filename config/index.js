'use strict';

import Dimensions from 'Dimensions';

const screen = Dimensions.get('window');

export default {
  SCREEN_WIDTH: screen.height,
  SCREEN_HEIGHT: screen.width,
  ASPECT_RATIO: screen.width / screen.height,
  LATITUDE_DELTA: 0.0972,
  LONGITUDE_DELTA: 0.0972 * (screen.width / screen.height),
  API_URL: 'http://47.75.42.15:8000/api',
  // API_URL: 'http://192.168.1.134:8000/api',
  GOOGLE_API_KEY: 'AIzaSyBX4HDFHTcYFxQZK0qGf-I_uOKICsmePac',
  USC_COORDINATES: {
    longitude: -118.285117,
    latitude: 34.022352,
  },
};
